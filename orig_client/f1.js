var token=""
// var BaseURL="http://matan.svgalib.org:8000/"
var BaseURL="http://localhost:8000/"
var deck='/standard/'
var gnumcards=-1
var playing=false

var HttpClient = function () {
  this.get = function (aUrl, aCallback) {
    var anHttpRequest = new XMLHttpRequest()
    anHttpRequest.onreadystatechange = function () {
      if (anHttpRequest.readyState === 4 && anHttpRequest.status === 200) { aCallback(anHttpRequest.responseText) }
    }

    anHttpRequest.open('GET', aUrl, true)
    anHttpRequest.send(null)
  }
}

function ignoreres(response) {
    return
}

function bidres(response) {
    console.log('bidres ', response)
    if(response=='0') {
        document.getElementById('bidbuttons').style.display = 'none'
        document.getElementById('unbidbutton').style.display = 'inline-block'
    } else {
    }
}

function autodeal() {
    var client = new HttpClient()
    client.get(BaseURL+'autodeal',ignoreres)
}
function switchdealer() {
    var client = new HttpClient()
    client.get(BaseURL+'switchdealer',ignoreres)
}
function bidclick(element,val) {
    var client = new HttpClient()
    client.get(BaseURL+'bid/'+token+'/'+val.toString(),bidres)
}

var cardclicked=-1;
function cardres(response) {
    console.log('cardres ', response)
    var res=response.split(" ")
    if (res[0]=='0')
        cardclicked.style.display = 'none'
    cardclicked=-1
}

var clickedpos=-1;

function cardclick(element,val,pos) {
    var client = new HttpClient()
    console.log('cardclick:', element.attributes.val.nodeValue, cardclicked, pos)
    if(playing) {
        if(cardclicked!=-1) return
        cardclicked=element
        client.get(BaseURL+'card/'+token+'/'+element.attributes.val.nodeValue.toString(),cardres)
    } else {
        if(clickedpos==-1) {
            cardclicked=element
            clickedpos=pos
        } else if(element!=cardclicked) {
            var s=element.outerHTML
            element.outerHTML=cardclicked.outerHTML
            cardclicked.outerHTML=s
            //var s=document.getElementById('cell_'+pos.toString()).innerHTML
            //document.getElementById('cell_'+pos.toString()).innerHTML=document.getElementById('cell_'+clickedpos.toString()).innerHTML
            //document.getElementById('cell_'+clickedpos.toString()).innerHTML=s
            clickedpos=-1
            cardclicked=-1
        }
    }

}

function unbid(element) {
    var client = new HttpClient()
    console.log('unbid clicked')
    document.getElementById('unbidbutton').style.display = 'none'
    client.get(BaseURL+'unbid/'+token,ignoreres)
}

function carddblclick(element,val) {
    var client = new HttpClient()
    console.log('carddblclick:', element.attributes.val.nodeValue, cardclicked, playing)
    if (!playing)
        return
    cardclicked=element
    client.get(BaseURL+'card/'+token+'/'+val.toString(),cardres)
}

function resetcardclicked() {
    cardclicked=-1
    playing=true
}

function eventloop(response) {
    var client = new HttpClient()
    console.log('eventloop ',response)
    document.getElementById('message').innerHTML=''
    if(response.startsWith('cards: ')) {
        var cards=response.split(" ")
        playing=false
        cardclicked=-1
        cards.sort(function(a,b){if(a=='cards:')return -1; return Number(b)-Number(a)})
        document.getElementById('cards').style.display = 'block'
        document.getElementById('shuffle_deal').style.display = 'none'
        var pos=[]
        for (var i=0;i<17;i++)pos[i]=i;
        var dist=[0,0,0,0]
        for(var i=1;i<cards.length;i++ ) {
            dist[~~(Number(cards[i])/13)]++
        }
        var od=dist.slice()
        od.sort(function(a,b){if(a=='cards:')return -1; return Number(b)-Number(a)})
        console.log(cards)
        console.log(dist, od)
        if(od[0]<=4) {
            var t=1
            var np=cards.length
            for(var i=3;i>=0;i--) {
                p=4-i
                for(var k=0;k<dist[i];k++){
                    pos[t++]=p
                    p+=4
                }
                for(var k=dist[i];k<4;k++){
                    pos[np++]=p
                    p+=4
                }
            }
            console.log(pos)
        } else {
            p=3
            var t=1
            var np=cards.length
            for(var k=1;k<=16;k++) {
                pos[t++]=p
                p+=4
                if(p>16) p-=17
            }
        }

        for(var i=1;i<cards.length;i++ ){
            var ez=""
            if(cards[i].length==1)ez="0"
            document.getElementById('cell_'+pos[i].toString()).innerHTML=
            '<button type="button" class="cardbutton"><img class="cardimg" src="http://my.svgalib.org/whist'+
            deck+ez+cards[i]+
            '.png" val="'+
            cards[i]+
            '" onclick="cardclick(this,'+
            cards[i]+
            ','+
            pos[i].toString()+
            ')" ondblclick="carddblclick(this,'+
            cards[i]+
            ')"></button>'
        }
        for(var i=cards.length; i<=15; i++)
            document.getElementById('cell_'+pos[i].toString()).innerHTML='<button type="button" class="cardbutton"><img class="cardimg" src="http://my.svgalib.org/whist/transparentcard.png" val=-1 onclick="cardclick(this,-1, '+pos[i].toString()+')"></button>'
    }
    if(response.startsWith('playing: ')) {
        var cards=response.split(" ")
        if(response=='playing: -1 -1 -1 -1') {
            document.getElementById('playing').style.display = 'none'
        } else {
            document.getElementById('playing').style.display = 'inline-block'
        }
        for(var i=1;i<=4;i++ ){
            if(cards[i]=='-1') {
                document.getElementById('playing_'+i.toString()).innerHTML='<img class="cardimg" src="http://my.svgalib.org/whist/transparentcard.png"></img>'
            } else {
                var ez=""
                if(cards[i].length==1)ez="0"
                document.getElementById('playing_'+i.toString()).innerHTML='<img class="cardimg" src="http://my.svgalib.org/whist'+deck+
                    ez+cards[i]+'.png"></img>'
            }
        }
    }
    if(response.startsWith('shuffle')) {
        document.getElementById('shuffle_deal').style.display = 'inline-block'
    }
    if(response.startsWith('turnnum: ')) {
        var names=response.split(" ")
        document.getElementById('turnnum').innerHTML='תור '+names[1]
        document.getElementById('bids_tot').innerHTML='מתוך '+names[2]
        document.getElementById('bids_tot').style.color = 'black'
        document.getElementById('bids_tot').style.fontWeight = "400"
        document.getElementById('token').style.display = 'none'
        gnumcards=Number(names[2])
        console.log('numcards: ', gnumcards)
    }
    if(response.startsWith('deck: ')) {
        var names=response.split(" ")
        deck=names[1]
    }
    if(response.startsWith('names: ')) {
        var names=response.split(" ")
        document.getElementById('toptable').style.display = 'inline-block'
        for(var i=1;i<=4;i++ ){
            document.getElementById('name_'+i.toString()).innerHTML=names[i]
            document.getElementById('pname_'+i.toString()).innerHTML=names[i]
        }
    }
    if(response.startsWith('nobid:')) {
        document.getElementById('bidbuttons').style.display = 'none'
    }
    if(response.startsWith('bids: ')) {
        var names=response.split(" ")
        var allbids=true
        var sum=0
        for(var i=1;i<=4;i++ ){
            sum+=Number(names[i])
            if(names[i]!='-1') {
                document.getElementById('bid_'+i.toString()).innerHTML=names[i]
            } else {
                allbids=false
                document.getElementById('bid_'+i.toString()).innerHTML=''
            }
        }
        console.log(allbids, gnumcards, sum)
        if(allbids) {
            playing=true
            cardclicked=-1
            document.getElementById('bids_tot').innerHTML=sum.toString()+' מתוך '+gnumcards.toString()
            if (gnumcards-sum==1) {
                document.getElementById('bids_tot').style.color = 'red'
            } else if(gnumcards>sum) {
                document.getElementById('bids_tot').style.color = 'darkred'
                document.getElementById('bids_tot').style.fontWeight = "1000"

            } else  if(gnumcards-sum==-1) {
                document.getElementById('bids_tot').style.color = 'lilac'
            } else {
                document.getElementById('bids_tot').style.color = 'purple'
                document.getElementById('bids_tot').style.fontWeight = "1000"
            }
        }

    }
    if(response.startsWith('canunbid: ')) {
        var names=response.split(" ")
        console.log('canunbid', token, names)
        if(token==names[1]) {
            console.log('show unbid')
            document.getElementById('unbidbutton').style.display = 'inline-block'
        } else {
            console.log('hide unbid')
            document.getElementById('unbidbutton').style.display = 'none'
        }
    }
    if(response.startsWith('message: ')) {
        var m=response.slice(9)
        document.getElementById('message').innerHTML=m
    }
    if(response.startsWith('points: ')) {
        var names=response.split(" ")
        for(var i=1;i<=4;i++ ){
            document.getElementById('pts_'+i.toString()).innerHTML=names[i]
        }
    }
    if(response.startsWith('tricks: ')) {
        var names=response.split(" ")
        for(var i=1;i<=4;i++ ){
            if(names[i]!='-1')
                document.getElementById('tricks_'+i.toString()).innerHTML=names[i]
            else
                document.getElementById('tricks_'+i.toString()).innerHTML=''
        }
    }
    if(response.startsWith('trump: ')) {
        var trump=response.split(" ")[1]
        document.getElementById('trump').innerHTML='<img src="http://my.svgalib.org/whist/s'+trump+'.png" style="height:100%;"></img>'
    }
    if(response.startsWith('bid: ')) {
        var numcards=Number(response.split(" ")[1])
        var disallowed=Number(response.split(" ")[2])
        document.getElementById('bidbuttons').style.display = 'block'
        var s=''
        for(var i=0;i<=numcards;i++ ){
            var d=''
            if(i==disallowed) d=' disabled'
            s=s+'<button type="button"'+d+' class="bidbutton" val='+i.toString()+' onclick="bidclick(this,'+i.toString()+')">'+i.toString()+'</button>'
        }
        document.getElementById('bidbuttons').innerHTML=s
    }
    client.get(BaseURL+'event/'+token,eventloop)
}

function hideplaying() {
    document.getElementById('playing').style.display='none'
}

function shuffle() {
    var client = new HttpClient()
    client.get(BaseURL+'shuffle',ignoreres)
}

function deal() {
    var client = new HttpClient()
    client.get(BaseURL+'deal',ignoreres)
}


function startgame(response) {
    var client = new HttpClient()
    console.log('startgame  ',response)
    if(response.startsWith('token: ')) {
        var s=response.split(" ")
        token=s[1].trim()
        if(token=='') {
            return
        }
        console.log('token: ',token)
        document.getElementById('login').style.display = 'none'
        document.getElementById('token').innerHTML = token
        client.get(BaseURL+'event/'+token,eventloop)
    }
}


function joingame() {
    var client = new HttpClient()
    console.log(document.getElementById('name').value)
    client.get(BaseURL+'start/'+document.getElementById('name').value,startgame)
}
