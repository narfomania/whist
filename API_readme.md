
Whist server API:
==================
Messages are sent in URL of request: {BaseURL}message

BaseURL=http://matan.svgalib.org:8000

Server response is the body of the reply. Some messages do not get response.

Cards are numbered 0-51, Each suit consecutively, lower to higher.


Document format:
----------------
Client message - Server response
Action



/start/<name> - token: <token>
Register for a game.

In case the client forgets the information, sending /start/token will result in
events including all information needed to resume.

Alternatively, can use: 
/start/<name>/<oldname>/<oldtoken> - token: <token>
continue a game with oldname and oldtoken


/start/obs+<name> - observertoken: <token>
An observer may register by starting the name with the string "obs+".


/shuffle 
Shuffle deck


/deal 
Deal deck


/card/<token>/<card> - <res> <card>
Player with given token plays card.
<res> is 0 for success and [1,2,3] for faulure (not your turn, not your card,
card not allowed)


/uncard/<token>
Player with given token retracts last played card, if allowed


/bid/<token>/<bid> - <res>
<0> for success, other for failure


/unbid/<token>
Player with given token retracts bid, if allowed



/event/<token> - <Event>: <Arguments>
Request event from server. Client should do this Continually from the receiving
the reply to "/start". 
Will block until an event is available.

Events:
-------
names: <name1> <name2> <name3> <name4>
Player names. Sent once at the start of the game.

cards: <card1> <card2> ...
A list of the player's cards (separated by spaces), unsorted

playing: <card1> ... <card4>
Four cards currently in play, in the same order as "names:". 
-1 for players who did not yet play a card.

deck: <deck name>
Suggestion for which deck of card to use. Card faces are available at 
http://my.svgalib.org/whist/<deck name>/s??.png
Where ?? is 00 to 51.

bids: <bid1> ... <bid4>
Four current bids. Same rules as "playing:"

nobid:
You may not bid.

canunbid: <token>
You may unbid, if this is your token.

message: <message>
Text to display.

points: <points1> ... <points4>
The number of points each player has

tricks: <tricks1> ... <tricks4>
The number of tricks each player won in this playing round.

trump: <trump>
Trump suit, 0..3.

bid: <numcards> <disallowed>
Request to submit a bid. numcards is the current number of cards. disallowed is
the disallowed bid, -1 if all allowed.

turnnum: <turn number> <cards number>

shuffle:
Player should shuffle and deal.

end: <points1> ... <points4>
Game over.

Observer events
---------------
allcards: <k=numcards> <player1card1> <player1card2> ... <player1cardk> <player2card1> ... <player4cardk>
A list of all players' cards (separated by spaces), unsorted
k=numcards is the number of cards for each player
