#!/usr/bin/python3

import os,sys
import math
import time
import string
import random

import urllib.request
import urllib.parse

starttime=time.time()

baseurl='http://my.svgalib.org/whist/server/'
baseurl="http://matan.svgalib.org:8000/"

bid_stdev=0.7

def pdebug(*argv):
    print(time.time()-starttime,": ",*argv,file=sys.stderr)

def get_url(url):
    url=baseurl+urllib.parse.quote(url)
    pdebug('url=',url)
    try:
        return urllib.request.urlopen(url, timeout=600).read().decode()
    except Exception as e:
        pdebug(e)
        sys.exit(1)
        return ''

name_base='מחשב'
ver='R0'
uid=random.choice(string.ascii_letters + string.digits)

name=name_base+'.'+ver+uid

url='start/'+name+'///'
r=get_url(url).strip()
if not r.startswith('token: '):
    print('Unexpected response ',r,". Can't start game.",file=sys.stderr)
    sys.exit(1)
token=r.split(' ')[1]

def roundup(x): 
    return int(math.ceil(abs(x))*x/abs(x)) if x is not 0 else 1 # Never return 0.

class Known:
    def __init__(self, numcards=1):
        self.numcards=numcards
        self.played=[]
        self.lead=-1
        self.playing=[-1,-1,-1,-1]
        self.bids=[-1,-1,-1,-1]
        self.overbid=0
        self.disallowed=-1

def bid(known):
    if known.disallowed==-1:
        mean=known.numcards/4
        bid=round(random.gauss(mean,bid_stdev))
    else:
        bid=known.disallowed+roundup(random.gauss(0,bid_stdev))
    if bid<0:
        bid=1 if known.disallowed==0 else 1
    if bid>known.numcards:
        bid=known.numcards-1 if known.disallowed==known.numcards else known.numcards
    url='bid/'+token+'/'+str(bid)
    r=get_url(url)


def playcard(known):
    if known.lead<0:
        cards=known.cards[:]
    else:
        cards=[x for x in known.cards if x//13==known.lead]
        if cards==[]:
            cards=known.cards[:]
    card=random.choice(cards)
    url='card/'+token+'/'+str(card)
    r=get_url(url)
    if r[0]=='0':
        known.cards.remove(int(r.split(' ')[1]))

while True:
    url='event/'+token
    r=get_url(url)
    pdebug('<== ',r)
    s=r.split(' ')
    command=s[0]
    if command=='turnnum:':
        known=Known(int(s[2]))
    if command=='trump:':
        known.trump=int(s[1])
    if command=='cards:':
        known.cards=[int(x) for x in s[1:]]
    if command=='playing:':
        known.playing=[int(x) for x in s[1:]]
        if known.playing.count(-1)==3:
            known.lead=[x//13 for x in known.playing if x>=0][0]
        if known.playing.count(-1)==0:
            known.played.append(known.playing)
    if command=='bids:':
        known.bids=[int(x) for x in s[1:]]
        if known.bids.count(-1)==0:
            known.overbid=sum(known.bids)-known.numcards
    if command=='bid:':
        known.disallowed=int(s[2])
        bid(known)
    if command=='card:':
        playcard(known)
    if command=='end:':
        sys.exit(0)







