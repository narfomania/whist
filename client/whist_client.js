var token="";
var BaseURL="https://my.svgalib.org/whist/server/";
var matanBaseURL="https://matan.svgalib.org:28000/";
var localBaseURL="https://localhost:28000/";
var deck='/standard/';
var gnumcards=-1;
var playing=false;
var trumpNames=["עלה Spade", "לב Heart", "Club תלתן", "Diamond יהלום"];
var trumpSymbols=["♠", "♥", "♣", "♦"];
var cardNames=["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"];

//observer fields
var observerMode=false;
var observedPlayer=1;
var allPlayerCards=[4];
var eventloop_timer_restart=1000;
var timer=false;
var stopEvents=false

var realGameID=''

function eventloopErrorTimer() {
    timer=setTimeout(eventlooperror, eventloop_timer_restart, 'timeout');
    eventloop_timer_restart+=eventloop_timer_restart;
    console.log("event loop error: ",eventloop_timer_restart);
    if (eventloop_timer_restart>3600000) eventloop_timer_restart=3600000;
}

var HttpClient = function () {
  this.get = function (aUrl, aCallback) {
    var anHttpRequest = new XMLHttpRequest();
    anHttpRequest.overrideMimeType("text/plain");
    if(aCallback==eventloop) {
        anHttpRequest.timeout=900000
        anHttpRequest.addEventListener('error', eventloopErrorTimer);
        anHttpRequest.addEventListener('timeout', eventloopErrorTimer);
        anHttpRequest.addEventListener('abort', eventloopErrorTimer);
    }
    anHttpRequest.onreadystatechange = function () {
      if (anHttpRequest.readyState === 4 && anHttpRequest.status === 200) {
          aCallback(anHttpRequest.responseText);
          eventloop_timer_restart=1000;
      }
    };

    anHttpRequest.open('GET', aUrl, true);
    anHttpRequest.send(null);
  };
};

function send_message(message, callback) {
    var client = new HttpClient();
    client.get(BaseURL+message,callback);
}

function ignoreres(response) {
    return;
}

function bidres(response) {
    console.log('bidres ', response);
    if(response==='0') {
        document.getElementById('bidbuttons').style.display = 'none';
        document.getElementById('unbidbutton').style.display = 'inline-block';
    } else {
    }
}

function autodeal() {
    if (observerMode)
        return;
    console.log('dblclick ',event.which)
    send_message('autodeal/'+token,ignoreres);
}
function switchdealer() {
    if (observerMode)
        return;
    send_message('switchdealer/'+token,ignoreres);
}
function bidclick(element,val) {
    if (observerMode)
        return;
    send_message('bid/'+token+'/'+val.toString(),bidres);
}

var cardclicked=-1;
function cardres(response) {
    console.log('cardres ', response);
    var res=response.split(" ");
    if (res[0]==='0')
        cardclicked.style.display = 'none';
    cardclicked=-1;
}

var clickedpos=-1;

function cardclick(element,val,pos) {
    console.log('cardclick:', element.attributes.val.nodeValue, cardclicked, val, pos);
    element.blur()
    if(playing) {
        if (observerMode)
            return;
        if(val==-1)
            return;
        if(cardclicked!==-1) return;
        cardclicked=element;
        send_message('card/'+token+'/'+element.attributes.val.nodeValue.toString(),cardres);
    } else {
        if(clickedpos===-1) {
            cardclicked=element;
            clickedpos=pos;
        } else if(element!==cardclicked) {
            var s=element.outerHTML;
            element.outerHTML=cardclicked.outerHTML;
            cardclicked.outerHTML=s;
            //var s=document.getElementById('cell_'+pos.toString()).innerHTML;
            //document.getElementById('cell_'+pos.toString()).innerHTML=document.getElementById('cell_'+clickedpos.toString()).innerHTML;
            //document.getElementById('cell_'+clickedpos.toString()).innerHTML=s;
            clickedpos=-1;
            cardclicked=-1;
        }
    }

}

function unbid(element) {
    console.log('unbid clicked');
    document.getElementById('unbidbutton').style.display = 'none';
    if (observerMode)
        return;
    send_message('unbid/'+token,ignoreres);
}

function carddblclick(element,val) {
    return
    console.log('carddblclick:', element.attributes.val.nodeValue, cardclicked, playing);
    if (!playing)
        return;
    if (observerMode)
        return;
    cardclicked=element;
    send_message('card/'+token+'/'+val.toString(),cardres);
}

function resetcardclicked() {
    console.log("resetcardclicke()")
    cardclicked=-1;
    playing=true;
}

function markPlayer(player) {
    for (var i=1;i<=4;i++) {
        if (i==player) {
            document.getElementById('name_' + i.toString()).style.backgroundColor = 'red';
            document.getElementById('pname_' + i.toString()).style.backgroundColor = 'red';
        } else {
             document.getElementById('name_' + i.toString()).style.removeProperty('background-color');
             document.getElementById('pname_' + i.toString()).style.removeProperty('background-color');
        }
    }
}

function keypress() {
    //console.log(event);
    if (token!="") { // Keys that should not be handled during game startup
        if (observerMode && event.key >="1" && event.key<="4") {
            observedPlayer=Number(event.key)-1;
            displayCards(allPlayerCards[observedPlayer]);
            markPlayer(observedPlayer+1)
        }
        if (event.code=='KeyH') {
            // hide/unhide cards
            console.log("hide")
            for (var i = 1; i <= 63; i++) {
                if (document.getElementById('cell_' + i.toString()).innerHTML.includes('cardback.png#')) {
                    document.getElementById('cell_' + i.toString()).innerHTML =
                    document.getElementById('cell_' + i.toString()).innerHTML.replace('cardback.png#','');
                } else if (!document.getElementById('cell_' + i.toString()).innerHTML.includes('transparentcard.png')){
                    document.getElementById('cell_' + i.toString()).innerHTML =
                    document.getElementById('cell_' + i.toString()).innerHTML.replace(deck,deck+'cardback.png#');
                }
                if(i==16) i=49;
                if(i==53) i=59;
            }
        }
        if (event.code=='KeyE') {
            // Restart event loop
            console.log("restart event loop")
            stopEvents=false
            eventloop('')
        }
        if (event.code=='KeyD') {
            switchdealer()
        }
        if (event.code=='KeyR') {
            resetcardclicked()
        }
        if (event.code=='KeyT') {
            console.log('Hide bids')
            for(i=1;i<=4;i++) {
            if(!document.getElementById('name_'+i.toString()).innerHTML.includes("שירי")) {
                    document.getElementById('bid_'+i.toString()).innerHTML=''
                }
            }
        }
    }
}

function getURL(response) {
    console.log("in getURL", response.trim())
    b=response.trim()
    if(b.startsWith('http')) {
        BaseURL=b
    } else if(b.startsWith(':')) {
        s=document.URL.split('/').slice(0,3)
        s[2]=s[2].split(':')[0]
        BaseURL=s[0]+'/'+s[1]+'/'+s[2]+b+'/'
    } else if(b=='') {
        s=document.URL.split('/').slice(0,3)
        s[2]=s[2].split(':')[0]
        BaseURL=s[0]+'/'+s[1]+'/'+s[2]+':28000/'
    }
    console.log('in getURL, BaseURL=',BaseURL)
}

function initall() {
    console.log('initall')
    var nameInput = document.getElementById("name");
    let params = new URLSearchParams(document.location.search.substring(1));
    console.log(document.URL, document.URL.startsWith("https://my.svgalib.org/whist/d/whist_client.html"), document.URL.startsWith("https://my.svgalib.org/whist/e/whist_client.html"))
    if(document.URL.startsWith("https://my.svgalib.org/whist/d/whist_client.html")) {
        BaseURL=matanBaseURL;
    } else if(document.URL.startsWith("https://my.svgalib.org/whist/e/whist_client.html")) {
        var client = new HttpClient();
        client.get("https://my.svgalib.org/whist/URL", getURL);
    } else {
        // Default server URL is port 28000 on the same server
        s=document.URL.split('/').slice(0,3)
        s[2]=s[2].split(':')[0]
        BaseURL=s[0]+'/'+s[1]+'/'+s[2]+':28000/'
        // But ask server
        var client = new HttpClient();
        client.get(document.URL.split('/').slice(0, -1).join('/')+"/URL", getURL);
    }

    console.log(params.get('gameId'), document.location.search.substring(1))
    if (params.get('gameId')) {
        document.getElementById("gameId").value=params.get('gameId')
    }

    if (typeof(Storage) !== "undefined") {
        oldname=localStorage.getItem('whist.name')
        if(oldname) {
            nameInput.value =
            nameInput.defaultValue = oldname;
            nameInput.select()
        }
    }
    nameInput.addEventListener("keyup", function (ev) {
        if (ev.key === "Enter") {
            ev.preventDefault();
            document.getElementById("startButton").click();
        }
    })

}

var win=null;

function fulltable() {
    if(!win) {
        win=window.open(BaseURL+'table.html?'+realGameID);
    } else {
        win=window.open(BaseURL+'table.html?'+realGameID);
        // Can't do that anyway:
        // win.location.reload()
        // win.focus()
    }

}

// allcards: <k=numcards> <player1card1> <player1card2> ... <player1cardk> <player2card1> ... <player4cardk>
// A list of all players' cards (separated by spaces), unsorted
// k=numcards is the number of cards for each player
// example- allcards: 3 0 1 2 3 4 5 6 7 8 9 10 11
// cards may be -1

function handleEventAllCards(response) {
    var splitResponse = response.split(" ");
    var numcards = parseInt(splitResponse[1]);
    if (splitResponse.length !== numcards * 4 + 2) {
        console.log("ignoring malformed event, wrong number of cards: "+response);
        return;
    }

    for (var player = 0; player < 4; player++) {
        var firstCard = 2 + player * numcards;
        var lastCard = firstCard + numcards - 1;
        allPlayerCards[player] = [];
        for(var i=firstCard; i<=lastCard; i++ ) {
            if (splitResponse[i]!=="-1") {
                allPlayerCards[player].push(splitResponse[i]);
            }
        }
    }

    if (0 <= observedPlayer && observedPlayer <= 3) {
        displayCards(allPlayerCards[observedPlayer]);
    }

}



function displayCards(cards) {
    cards.sort(function (a, b) {
        return Number(b) - Number(a)
    });
    document.getElementById('cards').style.display = 'block';
    document.getElementById('shuffle_deal').style.display = 'none';
    if (cards[0]=="") return;
    var pos = [];
    for (var i = 0; i < 16; i++) pos[i] = i+1;
    var dist = [0, 0, 0, 0];
    for (var i = 0; i < cards.length; i++) {
        dist[3-(~~(Number(cards[i]) / 13))]++;
    }
    let numpos=16
    if (document.documentElement.clientWidth/document.documentElement.clientHeight>2/3)
    { // Use extra columns
        colstarts=[60,50,4,3,2,1]
        splits=[dist[0]]
        for(i=1;i<3;i++)
            splits[i]=splits[i-1]+dist[i]
        console.log(splits)
        columnsneeded=0
        for(let i=0;i<4;i++) columnsneeded+=(~~((dist[i]+3)/4))
        console.log(columnsneeded,dist)
        if(columnsneeded<=4) {
            startpos=4
            for( let e of document.getElementsByClassName("column5")) e.innerHTML=""
            for( let e of document.getElementsByClassName("column6")) e.innerHTML=""
        } else if (columnsneeded==5) {
            startpos=50
            numpos=20
        } else if (columnsneeded==6) {
            startpos=60
            numpos=24
        }
        p = startpos;
        var t = 0;
        var np = cards.length;
        var placecard=true;
        for (var k = 1; k <= numpos; k++) {
            if(placecard)
                pos[t++] = p;
            else
                pos[np++] = p;
            if(p<50) {
                p += 4;
                if (p > 16) p -= 17;
            } else {
                p += 1;
                if(p==54) p=4;
                if(p==64) p=50;
            }
            if(splits.includes(t))
                placecard=false
            if(colstarts.includes(p))
                placecard=true
            if(t>=cards.length)
                placecard=false
        }
        console.log("pos=",pos)
    } else { // Only use four columns
        dist.reverse()
        var od = dist.slice();
        od.sort(function (a, b) {
            return Number(b) - Number(a)
        });
        if (od[0] <= 4) {
            var t = 0;
            var np = cards.length;
            for (var i = 3; i >= 0; i--) {
                p = 4 - i;
                for (var k = 0; k < dist[i]; k++) {
                    pos[t++] = p;
                    p += 4;
                }
                for (var k = dist[i]; k < 4; k++) {
                    pos[np++] = p;
                    p += 4;
                }
            }
        } else {
            p = 4;
            var t = 0;
            var np = cards.length;
            for (var k = 1; k <= 16; k++) {
                pos[t++] = p;
                p += 4;
                if (p > 16) p -= 17;
            }
        }
    }

    for (var i = 0; i < cards.length; i++) {
        var card = Number(cards[i]);
        var cardValue = card % 13;
        var trump = ~~(card / 13); //floor (integer division)
        document.getElementById('cell_' + pos[i].toString()).innerHTML =
            '<button type="button" class="cardbutton"><img class="cardimg" src="https://my.svgalib.org/whist' +
            deck + cards[i].padStart(2,'0') +
            '.png" val="' +
            cards[i] +
            '" onclick="cardclick(this,' +
            cards[i] +
            ',' +
            pos[i].toString() +
            ')" ondblclick="carddblclick(this,' +
            cards[i] +
            ')" alt="' + cardNames[cardValue] + trumpSymbols[trump] + '" title="' + cardNames[cardValue] + trumpSymbols[trump] + '" /></button>';
    }
    for (var i = cards.length; i < numpos; i++) {
        document.getElementById('cell_' + pos[i].toString()).innerHTML = '<button type="button" class="cardbutton"><img class="cardimg" src="https://my.svgalib.org/whist/transparentcard.png" val="-1" onclick="cardclick(this,-1, ' + pos[i].toString() + ')"></button>';
    }
}

// A list of the player's cards (separated by spaces), unsorted
// cards: <card1> <card2> ...
function handleEventCards(response) {
    var cards = response.split(" ");
    playing = false;
    cardclicked = -1;
    displayCards(cards.slice(1));
}

// playing: <card1> ... <card4>
// Four cards currently in play, in the same order as "names:".
// -1 for players who did not yet play a card.
function handleEventPlaying(response) {
    {
        var cards = response.split(" ");
        if (response === 'playing: -1 -1 -1 -1') {
            document.getElementById('playing').style.display = 'none';
        } else {
            document.getElementById('playing').style.display = 'inline-block';
        }
        for (var i = 1; i <= 4; i++) {
            if (cards[i] === '-1') {
                document.getElementById('playing_' + i.toString()).innerHTML = '<img class="cardimg" src="https://my.svgalib.org/whist/transparentcard.png" alt=""/>';
            } else {
                var card = Number(cards[i]);
                var cardValue = card % 13;
                var trump = ~~(card / 13); //floor (integer division)
                document.getElementById('playing_' + i.toString()).innerHTML = '<img class="cardimg" src="https://my.svgalib.org/whist' + deck +
                    cards[i].padStart(2,'0') + '.png" alt="' + cardNames[cardValue] + trumpSymbols[trump] + '" title="' + cardNames[cardValue] + trumpSymbols[trump] + '" />';
            }
        }
    }
}

// turnnum: <turn number> <cards number>
function handleEventTurnNum(response) {
    var names = response.split(" ");
    document.getElementById('turnnum').innerHTML = 'תור ' + names[1];
    document.getElementById('bids_tot').innerHTML = 'מתוך ' + names[2];
    document.getElementById('bids_tot').style.color = 'black';
    document.getElementById('bids_tot').style.fontWeight = "400";
    document.getElementById('tokendisplay').style.display = 'none';
    gnumcards = Number(names[2]);
    console.log('numcards: ', gnumcards);
}

// shuffle:
// Player should shuffle and deal.
function handleEventShuffle() {
    document.getElementById('shuffle_deal').style.display = 'block';
}

// deck: <deck name>
// Suggestion for which deck of card to use. Card faces are available at
// https://my.svgalib.org/whist/<deck name>/s??.png
// Where ?? is 00 to 51.
function handleEventDeck(response) {
    var names = response.split(" ");
    deck = names[1];
}

// names: <name1> <name2> <name3> <name4>
// Player names. Sent once at the start of the game.
function handleEventNames(response) {
    var names = response.split(" ");
    document.getElementById('toptable').style.display = 'inline-block';
    for (var i = 1; i <= 4; i++) {
        document.getElementById('name_' + i.toString()).innerHTML = names[i];
        document.getElementById('pname_' + i.toString()).innerHTML = names[i];
    }
}


// nobid:
// You may not bid.
function handleEventNoBid() {
    if (observerMode)
        return;
    document.getElementById('bidbuttons').style.display = 'none';
}


// bids: <bid1> ... <bid4>
// Four current bids. Same rules as "playing:"
function handleEventBids(response) {
    var names = response.split(" ");
    var allbids = true;
    var sum = 0;
    for (var i = 1; i <= 4; i++) {
        sum += Number(names[i]);
        if (names[i] != '-1') {
            document.getElementById('bid_' + i.toString()).innerHTML = names[i];
        } else {
            allbids = false;
            document.getElementById('bid_' + i.toString()).innerHTML = '';
        }
    }
    console.log(allbids, gnumcards, sum);
    if (allbids) {
        playing = true;
        cardclicked = -1;
        document.getElementById('bids_tot').innerHTML = sum.toString() + ' מתוך ' + gnumcards.toString();
        if (gnumcards - sum === 1) {
            document.getElementById('bids_tot').style.color = 'red';
        } else if (gnumcards > sum) {
            document.getElementById('bids_tot').style.color = 'darkred';
            document.getElementById('bids_tot').style.fontWeight = "1000";

        } else if (gnumcards - sum === -1) {
            document.getElementById('bids_tot').style.color = 'MediumOrchid';
        } else {
            document.getElementById('bids_tot').style.color = 'purple';
            document.getElementById('bids_tot').style.fontWeight = "1000";
        }
    }
}

// canunbid: <token>
// You may unbid, if this is your token.
function handleEventCanUBid(response) {
    var names = response.split(" ");
    if (observerMode)
        return;
    console.log('canunbid', token, names);
    if (token == names[1]) {
        console.log('show unbid');
        document.getElementById('unbidbutton').style.display = 'inline-block';
    } else {
        console.log('hide unbid');
        document.getElementById('unbidbutton').style.display = 'none';
    }
}

// message: <message>
// Text to display.
function handleEventMessage(response) {
    var m = response.slice(9);
    document.getElementById('message').innerHTML = m;
}

// points: <points1> ... <points4>
// The number of points each player has
function handleEventPoints(response) {
    var names = response.split(" ");
    for (var i = 1; i <= 4; i++) {
        ;
        document.getElementById('pts_' + i.toString()).innerHTML = names[i];
    }
}

// tricks: <tricks1> ... <tricks4>
// The number of tricks each player won in this playing round.
function handleEventTricks(response) {
    var names = response.split(" ");
    for (var i = 1; i <= 4; i++) {
        if (names[i] !== '-1')
            document.getElementById('tricks_' + i.toString()).innerHTML = names[i];
        else
            document.getElementById('tricks_' + i.toString()).innerHTML = '';
    }
}

// end: <points1> ... <points4>
// Game over.
function handleEventEnd(response) {
    var m = response.slice(9)
    document.getElementById('message').innerHTML = '<h2>סוף המשחק. מתן ניצח.</h2>'
    if(document.getElementById('name_1').innerHTML.includes("מתן")) {
        for(i=2;i<=4;i++) {
            document.getElementById('pts_'+i.toString()).innerHTML='-'+document.getElementById('pts_'+i.toString()).innerHTML
        }
    }
}

// trump: <trump>
// Trump suit, 0..3.
// -1 for hiding the trump
function handleEventTrump(response) {
    var trump = response.split(" ")[1];
    if(trump=="-1") {
        document.getElementById('trump').style.display='none';
    } else {
        document.getElementById('trump').innerHTML = '<img src="https://my.svgalib.org/whist/s' + trump + '.png" style="height:100%;" alt="' + trumpSymbols[trump] + '"/>';
        document.getElementById('trump').style.removeProperty('display');
    }
}

// bid: <numcards> <disallowed>
// Request to submit a bid. numcards is the current number of cards. disallowed is
// the disallowed bid, -1 if all allowed.
function handleEventBid(response) {
    var numcards = Number(response.split(" ")[1]);
    var disallowed = Number(response.split(" ")[2]);
    if (observerMode)
        return;
    document.getElementById('bidbuttons').style.display = 'block';
    var s = '';
    for (var i = 0; i <= numcards; i++) {
        var d = '';
        if (i === disallowed) d = ' disabled';
        var bidButtonClass = 'bidbutton';
        if (document.documentElement.clientWidth/document.documentElement.clientHeight <= 3/3) bidButtonClass = 'bidbuttonphone';
        s = s + '<button type="button"' + d + ' class="' + bidButtonClass + '" val=' + i.toString() + ' onclick="bidclick(this,' + i.toString() + ')">' + i.toString() + '</button>';
    }
    document.getElementById('bidbuttons').innerHTML = s;
}

function handleStopEvent(response) {
    var m = response.slice(6);
    stopEvents=true
    document.getElementById('message').innerHTML = m;
    document.getElementById('tokendisplay').style.display = 'none';
}

function eventlooperror(e) {
    console.log(e)
    eventloop('')
}

function eventloop(response) {
    console.log('eventloop ',response);

    if(response.startsWith('stop: ')) {
        handleStopEvent(response);
    }
    if(stopEvents) return;

    if (response!='' && !response.startsWith('card:'))
        document.getElementById('message').innerHTML='';
    send_message('event/'+token,eventloop);

    if(response.startsWith('cards: ')) {
        handleEventCards(response);
    }
    if(response.startsWith('allcards: ')) {
        handleEventAllCards(response);
    }
    if(response.startsWith('playing: ')) {
        handleEventPlaying(response);
    }
    if(response.startsWith('shuffle')) {
        handleEventShuffle();
    }
    if(response.startsWith('turnnum: ')) {
        handleEventTurnNum(response);
    }
    if(response.startsWith('deck: ')) {
        handleEventDeck(response);
    }
    if(response.startsWith('names: ')) {
        handleEventNames(response);
    }
    if(response.startsWith('nobid:')) {
        handleEventNoBid();
    }
    if(response.startsWith('bids: ')) {
        handleEventBids(response);
    }
    if(response.startsWith('canunbid: ')) {
        handleEventCanUBid(response);
    }
    if(response.startsWith('message: ')) {
        handleEventMessage(response);
    }
    if(response.startsWith('points: ')) {
        handleEventPoints(response);
    }
    if(response.startsWith('tricks: ')) {
        handleEventTricks(response);
    }
    if(response.startsWith('end: ')) {
        handleEventEnd(response);
    }
    if(response.startsWith('trump: ')) {
        handleEventTrump(response);
    }
    if(response.startsWith('bid: ')) {
        handleEventBid(response);
    }
}

function uncardres(response) {
    console.log('uncardres: ',response)
    var a=Number(response)
    if(a>=0) {
        var card=a.toString()
        for(pos=16;pos>0;pos--) {
            if(
                (document.getElementById('cell_'+pos.toString()).innerHTML=='') ||
                document.getElementById('cell_'+pos.toString()).innerHTML.includes('transparentcard')
                ) {
                    var cardValue = card % 13;
                    var trump = ~~(card / 13); //floor (integer division)
                    document.getElementById('cell_'+pos.toString()).innerHTML=
                        '<button type="button" class="cardbutton"><img class="cardimg" src="https://my.svgalib.org/whist'+
                        deck+card.padStart(2,'0')+
                        '.png" val="'+
                        card+
                        '" onclick="cardclick(this,'+
                        card+
                        ','+
                        pos.toString()+
                        ')" ondblclick="carddblclick(this,'+
                        card+
                        ')" alt="'+cardNames[cardValue]+trumpSymbols[trump]+'" title="'+cardNames[cardValue]+trumpSymbols[trump]+'" /></button>';
                    return ;
            }
        }
    }
}

function hideplaying() {
    // Undo last card played
    console.log('unbid clicked');
    document.getElementById('unbidbutton').style.display = 'none';
    send_message('uncard/'+token,uncardres);
    //document.getElementById('playing').style.display='none';
}

function shuffle() {
    send_message('shuffle/'+token,ignoreres);
}

function deal() {
    send_message('deal/'+token,ignoreres);
}


function startgame(response) {
    console.log('startgame  ',response);
    if(response.startsWith('token: ') || response.startsWith('observertoken: ')) {
        var s=response.split(" ");
        token=s[1].trim();
        if(token==='') {
            return;
        }
        realGameID=s[2].trim()
        if(response.startsWith('token: ')) {
            document.getElementById('showGameID').innerHTML = '<a href="'+document.location.href.split('?')[0]+'?'+'gameId='+realGameID +
            '" onclick="return window.event.which==3;">'+realGameID+' :קוד משחק</a>';
        }
        console.log(s[0],token,realGameID);
        document.getElementById('login').style.display = 'none';
        document.getElementById('gameIdDiv').style.display = 'none';
        document.getElementById('token').innerHTML = token;
        send_message('event/'+token,eventloop);
        if (typeof(Storage) !== "undefined") {
            sessionStorage.setItem('whist.token',token);
        }
        observerMode = response.startsWith('observertoken: ');
        if(observerMode) markPlayer(observedPlayer+1);
    }
}


function joingame() {
    console.log(document.getElementById('name').value)
    var name=document.getElementById('name').value
    var gameId=document.getElementById('gameId').value
    var oldname
    var oldtoken
    if (typeof(Storage) !== "undefined") {
        oldname=localStorage.getItem('whist.name')
        oldtoken=sessionStorage.getItem('whist.token')
    }
    if (name!='') {
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem('whist.name',name)
        }
    }

    var url='start/'+name+'/'+oldname+'/'+oldtoken+'/'+gameId

    send_message(url,startgame);
}
