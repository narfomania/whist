#!/usr/bin/python3

import codecs
import getopt
import pickle
import queue
import random
import secrets
import sys
import threading
import time
import urllib
import ssl

#sys.stdout.reconfigure(encoding='utf-8')
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

allowed_names=['xyz', 'עירא', 'יהלי', 'רמון', 'תמנע', 'שירי', 'כפיר', 'אבא', 'מחשב', 'דרור', 'שקד']
trump_names=['Spade', 'Heart', 'Club', 'Diamond']
pregame_timeout=1200
ingame_timeout=7200

starttime=time.time()

defaultgameid=secrets.token_urlsafe(4)
observergameid=defaultgameid

def pdebug(*argv):
    if debug:
        print(time.time()-starttime,": ",*argv,file=logfile,flush=True)

def gamepickle(game, name="whist.pickle", wfile=None):
    if game:
        q=[x.queue for x in game.players]
        o=game.observers
        for x in game.players:
            x.queue=""
        game.observers=[]
        if not wfile:
            wfile=open(name,"wb")
        pickle.dump(game,wfile)
        for i in range(4):
            game.players[i].queue=q[i]
        game.observers=o

def observersqueue(game, m):
    for o in game.observers:
        game.observers[o].put(m)

def sendobservercards(game):
    if game.observers=={} or not game.turn or not game.turn.cards:
        return
    if game.turn and game.turn.numcards>0:
        m='allcards: '+str(game.turn.numcards)+' '
        n=' '.join([' '.join([str(x) for x in c+[-1]*(game.turn.numcards-len(c))]) for c in game.turn.cards])
        observersqueue(game,m+n)

def yourturn(game, to, you=True, disallowed=-1):
    if you:
        if game.players[to].name=='עירא' and random.randint(0,9)==0:
            game.players[to].queue.put('message: תורך, נודניק')
        else:
            game.players[to].queue.put('message: תורך')
    for i in range(4):
        m='message: תור '+game.players[to].name
        if disallowed>=0:
            m+='. אסור '+str(disallowed)
        if i!=to:
            game.players[i].queue.put(m)
        if i==0:
            observersqueue(game,m)

def queuestuff(game, name, stuff):
    if stuff:
        l=stuff
    else:
        l=[-1,-1,-1,-1]
    m=name+': '+' '.join([str(x) for x in l])
    for i in range(4):
        game.players[i].queue.put(m)
    observersqueue(game,m)

def destroy_game(gameid):
    global defaultgameid
    global observergameid
    pdebug('destroy ', gameid)
    pdebug('before games=', games, 'players=', players, 'observers=',observers, 'tokens=',tokens)
    if gameid in games:
        game=games[gameid]
        del games[gameid]
        if game.token in games:
            del games[game.token]
        for i in game.players:
            i.queue.put('stop: המשחק נזנח')
            del tokens[i.token]
        for i in game.observers.keys():
            i.put('stop: המשחק נזנח')
            del tokens[i]
    if gameid in players:
        for q in players[gameid]:
            del tokens[q.token]
            q.queue.put('stop: המשחק נזנח')
        del players[gameid]
    if gameid in observers:
        for q in observers[gameid]:
            observers[gameid][q].put('stop: המשחק נזנח')
            del tokens[q]
        del observers[gameid]
    if gameid in timers:
        timers[gameid].cancel()
        del timers[gameid]
    if gameid==defaultgameid:
        defaultgameid=secrets.token_urlsafe(4)
        observergameid=defaultgameid
    pdebug('after games=', games, 'players=', players, 'observers=',observers, 'tokens=',tokens)

def pregame_watchdog(gameid, timeout=pregame_timeout):
    pdebug('pregame_watchdog', gameid)
    if gameid in timers:
        timers[gameid].cancel()
    timers[gameid]=threading.Timer(timeout, destroy_game, [gameid])
    timers[gameid].start()

def ingame_watchdog(gameid):
    pregame_watchdog(gameid, timeout=ingame_timeout)

def announce(a, game=None):
    threading.Thread(target=real_announce,args=[a,game]).start()

def real_announce(a, game=None):
    pdebug('===========================================\n'+a)
    if a=='end':
        queuestuff(game, 'end', game.points)
        time.sleep(10)
        gamepickle(game, name='whist.pickle-'+game.token)
        time.sleep(3600)
        destroy_game(game.id)
    else:
        ingame_watchdog(game.id)
    if a=='names':
        pdebug([x.name for x in game.players])
        queuestuff(game, 'names', [x.name for x in game.players]) 
        queuestuff(game, 'turnnum', [ 1, 1, -1, -1 ])
    if a=='dealt':
        pdebug(game.turn.cards)
        for i in range(4):
            game.players[i].queue.put('cards: '+' '.join([str(x) for x in game.turn.cards[i]]))
            #game.players[i].queue.put('trump: '+str(game.turn.trump))
        sendobservercards(game)
        queuestuff(game, 'trump',[game.turn.trump])
        queuestuff(game, 'playing', None)
        game.players[game.turn.tobid].queue.put('bid: '+str(game.turn.numcards)+' -1')
        yourturn(game, game.turn.tobid, False)
    if a=='unbid':
        pdebug(game.turn.bids)
        queuestuff(game, 'bids', game.turn.bids)
        if len([x for x in game.turn.bids if x==-1])==1:
            disallowed=game.turn.numcards-sum(game.turn.bids)-1
        else:
            disallowed=-1
        game.players[game.turn.tobid].queue.put('bid: '+str(game.turn.numcards)+' '+str(disallowed))
        yourturn(game, game.turn.tobid, False, disallowed=disallowed)
        game.players[(game.turn.tobid+1)%4].queue.put('nobid:')
        game.players[(game.turn.tobid+1)%4].queue.put('message: תור '+game.players[game.turn.tobid].name)
    if a=='bid':
        pdebug(game.turn.bids)
        queuestuff(game, 'bids', game.turn.bids)
        queuestuff(game, 'canunbid', [game.players[game.turn.tobid-1].token])
        if not game.turn.bidalready:
            game.turn.bidalready=True
        if len([x for x in game.turn.bids if x==-1])==1:
            disallowed=game.turn.numcards-sum(game.turn.bids)-1
        else:
            disallowed=-1
        game.players[game.turn.tobid].queue.put('bid: '+str(game.turn.numcards)+' '+str(disallowed))
        yourturn(game, game.turn.tobid, False, disallowed=disallowed)
    if a=='bids':
        pdebug('end bids')
        pdebug(game.turn.cards)
        pdebug(game.turn.bids)
        pdebug('to play: ',game.turn.toplay)
        queuestuff(game, 'bids', game.turn.bids)
        queuestuff(game, 'canunbid', [game.players[game.turn.firstbid-1].token])
        game.players[game.turn.toplay].queue.put('card:')
        yourturn(game, game.turn.toplay)
    if a=='play':
        pdebug(game.turn.playing)
        if game.turn.playing.count(-1)==3 and game.turn.tricks==[0,0,0,0]:
            queuestuff(game, 'canunbid', [-1])
        queuestuff(game, 'playing', game.turn.playing)
        if -1 in game.turn.playing:
            game.players[game.turn.toplay].queue.put('card:')
            yourturn(game, game.turn.toplay)
        sendobservercards(game)
    if a=='played':
        pdebug(game.turn.tricks)
        try:
            queuestuff(game, 'playing', game.turn.played[-1][0])
        except:
            try:
                queuestuff(game, 'playing', game.turn.played[-1][-1][0])
            except:
                pass
        queuestuff(game, 'tricks', game.turn.tricks)
        if sum(game.turn.tricks)<game.turn.numcards:
            yourturn(game, game.turn.toplay)
            game.players[game.turn.toplay].queue.put('card:')
        sendobservercards(game)
    if a=='turned':
        pdebug(game.turn.bids)
        pdebug(game.turn.tricks)
        pdebug(game.points)
        queuestuff(game, 'trump',[-1])
        queuestuff(game, 'points', game.points)
        game.nextturn()
    if a=='shuffle':
        queuestuff(game, 'playing', None)
        queuestuff(game, 'tricks', None)
        queuestuff(game, 'bids', None)
        queuestuff(game, 'turnnum', [ game.turn_num, game.turn.numcards ])
    if a=='newturn':
        queuestuff(game, 'playing', None)
        queuestuff(game, 'tricks', None)
        queuestuff(game, 'bids', None)
    if a=='needdeal':
        game.dealer.queue.put('shuffle')


def turn2numcards(t):
    if t<=13:
        return t
    else:
        return 27-t

def turn2firstbid(t):
    return (t-1)%4

def tricks2points(t,b,n):
    if t==b:
        return t+5
    else:
        return -abs(t-b)


class Player:
    def __init__(self, name):
        self.name=name
        self.token=secrets.token_urlsafe(3)
        self.queue=queue.Queue()
        self.queue_waiting=0


players=dict() # While starting game, players[gameid] holds players registered to gameid.
observers=dict() # While starting game, observers[gameid] holds observerss registered to gameid.
tokens=dict() # All players and observers, dict[token] is the game the token participates in.
games=dict() # games[gameid] and games[gametoken] is the game with these id and token.
timers=dict()

class Turn:
    def __init__(self,game,numcards,firstbid,trump):
        self.deck=list(range(52))
        self.numcards=numcards
        self.game=game
        self.firstbid=firstbid
        self.tobid=-2
        self.trump=trump
        self.bids=[-1,-1,-1,-1]
        self.toplay=-1
        self.lead=-1
        self.playing=[-1,-1,-1,-1]
        self.tricks=[0,0,0,0]
        self.cards=None
        self.played=[]
        self.bidalready=False
        pdebug('firstbid='+str(firstbid))

    def shuffle(self):
        if self.cards!=None:
            return 1
        random.shuffle(self.deck)
        announce('shuffle',self.game)
        return 0

    def deal(self):
        if self.tobid!=-2:
            return 1
        self.tobid=self.firstbid
        self.cards=[[],[],[],[]]
        for i in range(self.numcards):
            for j in range(4):
                self.cards[j].append(self.deck.pop())
        announce("dealt",self.game)

    def bid(self,playernum,bid):
        if self.tobid!=playernum:
            return 1
        # TODO Option: allow impossible bids
        if not isinstance(bid,int) or bid<0 or bid>self.numcards:
            return 2
        self.bids[playernum]=bid
        if all([x>-1 for x in self.bids]):
            if sum(self.bids)==self.numcards:
                self.bids[playernum]=-1
                return 3
            m=max(self.bids)
            i=(self.tobid+1)%4
            while(self.bids[i]!=m):
                i=(i+1)%4
            self.tobid=-1
            self.toplay=i
            announce('bids',self.game)
        else:
            self.tobid=(self.tobid+1)%4
            announce('bid',self.game)
        return 0

    def undo_bid(self, playernum):
        if self.bids==[-1,-1,-1,-1]:
            return 2
        if self.toplay>-1 and self.playing==[-1,-1,-1,-1] and self.tricks==[0,0,0,0]:
            if (self.firstbid-1)%4!=playernum:
                return 1
            self.toplay=-1
        elif self.tobid!=(playernum+1)%4:
            return 1
        self.tobid=playernum
        self.bids[playernum]=-1
        announce('unbid', self.game)
        return 0

    def undo_card(self,playernum):
        if self.playing[playernum]==-1 or (self.toplay-1)%4!=playernum:
            return -1
        self.toplay=playernum
        oldcard=self.playing[playernum]
        if oldcard==-1:
            return -2
        self.playing[playernum]=-1
        self.cards[playernum].append(oldcard)
        if self.playing==[-1,-1,-1,-1]:
            self.lead=-1
        announce('play', self.game)
        return oldcard

    def play(self,playernum,card):
        if self.toplay!=playernum:
            return 1
        if not card in self.cards[playernum]:
            return 2
        if self.lead==-1:
            self.lead=card//13
        # TODO Option: allow illegal plays
        elif card//13!=self.lead and any([x//13==self.lead for x in self.cards[playernum]]):
            return 3
        self.playing[playernum]=card
        self.cards[playernum].remove(card)
        self.toplay=(self.toplay+1)%4
        if all([x>-1 for x in self.playing]):
            tp=[x for x in self.playing if x//13==self.trump]
            if tp:
                wc=max(tp)
            else:
                wc=max([x for x in self.playing if x//13==self.lead])
            winner=self.playing.index(wc)
            pdebug("played: ",self.playing, self.lead, self.trump, winner, tp)
            self.tricks[winner] += 1
            self.played.append([self.playing,self.toplay,winner,self.trump])
            self.toplay=winner
            self.lead=-1
            announce('played',self.game)
            self.playing=[-1,-1,-1,-1]
            if sum(self.tricks)==self.numcards:
                self.points=[ tricks2points(self.tricks[i],self.bids[i],self.numcards) for i in range(4) ]
                self.game.points=[self.game.points[i]+self.points[i] for i in range(4) ]
                self.game.table.append([[self.game.points[i],self.bids[i]] for i in range(4)])
                self.game.played.append(self.played)
                self.toplay=-1
                announce('turned',self.game)
        else:
            announce('play',self.game)
        return 0
 

class Game:
    def __init__(self, id='', seed=None):
        random.seed(seed)
        self.id=id
        self.token=secrets.token_urlsafe(8)
        self.trumps=[ random.randint(0,3) for x in range(26) ]
        self.turn_num=0
        self.num_turns=26
        self.turn=None
        self.played=[]
        self.players=[]
        self.observers=[]
        self.table=[]
        self.points=[0,0,0,0]
        self.turn=None
        self.autodeal=True
        self.dealer=None

    def nextturn(self):
        self.turn_num=self.turn_num+1
        if self.turn_num>self.num_turns:
            announce('end',self)
            return
        self.turn=Turn(self,turn2numcards(self.turn_num),turn2firstbid(self.turn_num),self.trumps[self.turn_num-1])
        if self.autodeal:
            if self.turn_num>1: # No reason to wait on the first turn.
                time.sleep(3)
            self.turn.shuffle()
            self.turn.deal()
        else:
            announce("needdeal",self)



from http.cookies import SimpleCookie
try:
    from http.server import ThreadingHTTPServer, BaseHTTPRequestHandler
except:
    from socketserver import ThreadingMixIn
    from http.server import HTTPServer, BaseHTTPRequestHandler

    class ThreadingHTTPServer(ThreadingMixIn, HTTPServer):
        daemon_threads = True

class WhistHTTPRequestHandler(BaseHTTPRequestHandler):
    def version_string(self):
        return 'WhistServer/3.0'
    def log_message(self, format, *args):
        return
    def do_GET(self):
        global defaultgameid
        global observergameid
        path=self.requestline.split(' ')[1].strip()
        pdebug(path)
        self.send_response(200)
        self.send_header('Access-Control-Allow-Origin','*')
        if path.startswith('/table.html'):
          s=path.split('?')
          gameid=s[1] if len(s)>1 and s[1] else defaultgameid 
          game=games[gameid] if gameid in games else None
          if game and len(game.players)==4:
            self.send_header('Content-Type', 'text/html; charset=UTF-8')
            #self.send_header('')
            self.end_headers()
            self.wfile.write('<html dir="rtl"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><title>הטבלה לא משקרת</title></head>\n'.encode('utf-8'))
            self.wfile.write('<body> <table border=1>\n'.encode('utf-8'))
            self.wfile.write('<tr><th colspan=2></th>'.encode('utf-8'))
            for i in game.players:
                self.wfile.write(('<th colspan=2>'+i.name+'</th>').encode('utf-8'))
            self.wfile.write('</tr>\n'.encode('utf-8'))
            for i in range(1,game.num_turns+1):
                self.wfile.write(('<tr><td><img src="http://my.svgalib.org/whist/s'+str(game.trumps[i-1])+'.png" style="height:2vh;" alt="'+trump_names[game.trumps[i-1]]+'"></img></td>').encode('utf-8'))
                self.wfile.write(('<td>'+str(turn2numcards(i))+'</td>').encode('utf-8'))
                if i<game.turn_num:
                    for j in range(4):
                        self.wfile.write(('<td dir="rtl">'+str(game.table[i-1][j][1])+'</td>').encode('utf-8'))
                        self.wfile.write(('<td dir="rtl">'+str(game.table[i-1][j][0])+'</td>').encode('utf-8'))
                elif i==game.turn_num:
                    for j in range(4):
                        if(game.turn.bids[j]==-1):
                            self.wfile.write(('<td></td>').encode('utf-8'))
                        else:
                            self.wfile.write(('<td dir="rtl">'+str(game.turn.bids[j])+'</td>').encode('utf-8'))
                        self.wfile.write(('<td></td>').encode('utf-8'))
                else:
                    for j in range(4):
                        self.wfile.write(('<td></td>').encode('utf-8'))
                        self.wfile.write(('<td></td>').encode('utf-8'))
                self.wfile.write('</tr>\n'.encode('utf-8'))
            self.wfile.write('<table></body></html>'.encode('utf-8'))
            return
        self.send_header('Content-Type', 'text/plain; charset=UTF-8')
        try:
            args=urllib.parse.unquote(path).split('/')
            command=args[1]
            token=args[2]
        except:
            self.end_headers()
            return
        if command=='start':
            path=urllib.parse.unquote(path[7:])
            split=path.split('/')
            name=split[0]
            if name.startswith('obs+'):
                gameid=split[3] if len(split)>3 and split[3] else observergameid 
                game=games[gameid] if gameid in games else None
                token=secrets.token_urlsafe(3)
                if game:
                    tokens[token]=game
                    game.observers[token]=queue.Queue()
                    game.observers[token].put('names: '+' '.join( [x.name for x in game.players]))
                    game.observers[token].put('points: '+' '.join([str(x) for x in game.points]))
                    if game.turn:
                        game.observers[token].put('trump: '+str(game.turn.trump))
                        game.observers[token].put('turnnum: '+' '.join([str(x) for x in [ game.turn_num, game.turn.numcards, -1, -1 ]]))
                        if game.turn.cards:
                            sendobservercards(game)
                            game.observers[token].put('bids: '+' '.join([str(x) for x in game.turn.bids]))
                            if game.turn.tricks!=[0,0,0,0]:
                                game.observers[token].put('tricks: '+' '.join([str(x) for x in game.turn.tricks]))
                            if game.turn.playing!=[-1,-1,-1,-1]:
                                game.observers[token].put('playing: '+' '.join([str(x) for x in game.turn.playing]))
                else:
                    if not gameid in observers:
                        observers[gameid]=dict()
                        pregame_watchdog(gameid)
                    observers[gameid][token]=queue.Queue()
                    tokens[token]=observers[gameid][token]
                self.end_headers()
                self.wfile.write(('observertoken: '+token+' '+game.id).encode())
                return
            # Not 'obs+':
            #cookies = SimpleCookie(self.headers.get('Cookie'))
            #pdebug('cookies: ', cookies)
            #pdebug(self.headers)
            gameid=split[3] if len(split)>3 and split[3] else defaultgameid 
            game=games[gameid] if gameid in games else None
            pdebug('split=',split,'tokens=',tokens)
            if name in tokens:
                game=tokens[name]
            elif len(split)>=3 and split[2] in tokens:
                game=tokens[split[2]]
            if game:
                self.end_headers()
                token=name
                try:
                    if not game.turn:
                        return
                except AttributeError:
                    pdebug("game is ", type(game))
                    return
                if not token in game.tp:
                    #if 'token' in cookies:
                        #token=cookies['token']
                    if len(split)>=3:
                        token=split[2]
                    if not token in game.tp:
                        return
                p=game.tp[token]
                self.wfile.write(('token: '+token+' '+game.id).encode())
                for _ in range(game.players[p].queue_waiting+1):
                    game.players[p].queue.put('') # clear queue
                game.players[p].queue.put('names: '+' '.join( [x.name for x in game.players]))
                game.players[p].queue.put('points: '+' '.join([str(x) for x in game.points]))
                game.players[p].queue.put('names: '+' '.join( [x.name for x in game.players]))
                game.players[p].queue.put('trump: '+str(game.turn.trump))
                game.players[p].queue.put('turnnum: '+' '.join([str(x) for x in [ game.turn_num, game.turn.numcards, -1, -1 ]]))
                if game.turn.cards:
                    game.players[p].queue.put('cards: '+' '.join([str(x) for x in game.turn.cards[p]]))
                    game.players[p].queue.put('bids: '+' '.join([str(x) for x in game.turn.bids]))
                    if game.turn.bids!=[-1,-1,-1,-1] and ( 
                        (-1 in game.turn.bids and (p+1)%4==game.turn.tobid ) or \
                        (not -1 in game.turn.bids and (p+1)%4==game.turn.firstbid and game.turn.tricks==[0,0,0,0] and game.turn.playing==[-1,-1,-1,-1])):
                            queuestuff(game, 'canunbid', [token])
                    if game.turn.tricks!=[0,0,0,0]:
                        game.players[p].queue.put('tricks: '+' '.join([str(x) for x in game.turn.tricks]))
                    if game.turn.playing!=[-1,-1,-1,-1]:
                        game.players[p].queue.put('playing: '+' '.join([str(x) for x in game.turn.playing]))
                elif not game.autodeal and game.players[p]==game.dealer:
                    game.dealer.queue.put('shuffle')
                if game.turn.tobid==p:
                    if game.turn.bids.count(-1)==1:
                        disallowed=game.turn.numcards-sum(game.turn.bids)-1
                    else:
                        disallowed=-1
                    game.players[game.turn.tobid].queue.put('bid: '+str(game.turn.numcards)+' '+str(disallowed))
                else:
                    game.players[p].queue.put('nobid:')
                if game.turn.toplay==p:
                    yourturn(game, game.turn.toplay)
                return
            # Starting a new game
            pdebug(' in /start: gameid=', gameid)
            if gameid[:2].upper()=='4X':
                name=name[:12]
            else:
                if not any([x in name for x in allowed_names ]):
                    #if 'name' in cookies:
                        #name=cookies['name']
                    if len(split)>=2:
                        name=split[1]
                        if not any([x in name for x in allowed_names ]):
                            return
                    else: return
                if not 'מחשב' in name:
                    name=[x for x in allowed_names if x in name ][0]
                if name=='xyz':
                    name='מתן'
            self.send_header('Set-Cookie', 'name='+urllib.parse.quote(name)+'; Max-Age=5184000; Path=/')
            pdebug('name=',name)
            if not gameid in players:
                players[gameid]=[]
                pregame_watchdog(gameid)
            g_players=players[gameid]
            if len(g_players)<4:
                pl=Player(name)
                self.send_header('Set-Cookie', 'token='+pl.token+'; Max-Age=604800; Path=/')
                self.end_headers()
                g_players.append(pl)
                tokens[pl.token]=pl.queue # Queue until game is created.
                self.wfile.write(('token: '+pl.token+' '+gameid).encode())
                for i in g_players:
                    i.queue.put('message: נא להמתין. שחקנים: '+', '.join([x.name for x in g_players]))
                if len(g_players)==4:
                    if gameid==defaultgameid:
                        observergameid=defaultgameid
                        defaultgameid=secrets.token_urlsafe(4)
                    i_m=[i for i in range(4) if g_players[i].name=='מתן']
                    if i_m!=[] and i_m!=[0]:
                        g_players[0],g_players[i_m[0]] = g_players[i_m[0]],g_players[0]
                    i_s=[i for i in range(4) if g_players[i].name=='שירי']
                    if i_s!=[]:
                        i_r=[i for i in range(4) if g_players[i].name=='רמון']
                        if i_r!=[] and not (1 in i_s and 2 in i_r):
                            if not 1 in i_s:
                                g_players[1],g_players[i_s[0]] = g_players[i_s[0]],g_players[1]
                            i_r=[i for i in range(4) if g_players[i].name=='רמון']
                            if not 2 in i_r:
                                g_players[2],g_players[i_r[0]] = g_players[i_r[0]],g_players[2]
                    game=Game(gameid)
                    game.players=g_players
                    game.observers=observers[gameid] if gameid in observers else dict()
                    del players[gameid]
                    if gameid in observers:
                        del observers[gameid]
                    game.tp=dict()
                    for i in range(4):
                        game.tp[g_players[i].token]=i
                    for i in game.players:
                        tokens[i.token]=game
                    for i in game.observers.keys():
                        tokens[i]=game
                    i_s=[i for i in range(4) if g_players[i].name=='שירי']
                    i_s = []
                    if i_s!=[]:
                        game.autodeal=False
                        game.dealer=g_players[i_s[0]]
                        g_players[i_s[0]].queue.put('deck: /standard-left/')
                    announce('names', game)
                    games[game.token]=game
                    games[game.id]=game
                    threading.Thread(target=game.nextturn).start()
        else:
            self.end_headers()

        if token in tokens:
            game=tokens[token]
            if isinstance(game,queue.Queue):
                game=None
        else:
            if token in games:
                game=games[token]
            else:
                game=None
            if not command in ['event', 'tokens', 'pickle', 'unpickle', 'setturn']:
                return

        if command=='pickle':
            if game==None:
                game=list(games.values())[0]
            gamepickle(game)
        if command=='fpickle':
            if game==None:
                game=list(games.values())[0]
            gamepickle(game, wfile=self.wfile )
        elif command=='unpickle':
            game=pickle.load(open('whist.pickle',"rb"))
            game.observers=dict()
            for i in range(4):
                game.players[i].queue=queue.Queue()
                game.players[i].queue_waiting=0
            games[game.token]=game
            games[game.id]=game
            for i in game.players:
                tokens[i.token]=game
            for i in game.observers.keys():
                tokens[i]=game
        elif command=='tokens':
            done=[]
            for game in games.values():
                if not game in done:
                    self.wfile.write(("game token: "+game.token+"  game id: "+game.id+"\n").encode('utf-8'))
                    for i in game.players:
                        self.wfile.write((i.name+' '+i.token+'\n').encode('utf-8'))
                    self.wfile.write(("-----------------------------------------\n").encode('utf-8'))
                    done.append(game) # Every game appears twice in games.
        elif command=='setturn':
            t=int(args[3])
            if t>0 and t<=game.num_turns:
                game.points=[int(x) for x in args[4:8]]
                game.turn_num=t-1
                announce('turned',game)
                announce('newturn',game)
        elif command=='shuffle':
            if not game or not game.turn or game.turn.bidalready:
                return
            game.turn.shuffle()
        elif command=='deal':
            if not game or not game.turn or game.turn.bidalready:
                return
            game.turn.deal()
        elif command=='unbid':
            game.turn.undo_bid(game.tp[token])
        elif command=='uncard':
            self.wfile.write(str(game.turn.undo_card(game.tp[token])).encode())
        elif command=='bid':
            bid=args[3]
            try:
                bid=int(bid)
            except:
                bid=-1
            if bid>=0:
                self.wfile.write((str(game.turn.bid(game.tp[token],bid))).encode())
        elif command=='card':
            card=args[3]
            if not token in game.tp:
                self.wfile.write(("-4 "+card).encode())
                return
            try:
                card=int(card)
            except:
                card=-1
            if card>=0:
                #print('card ',game.tp[token],card)
                self.wfile.write((str(game.turn.play(game.tp[token],card))+" "+str(card)).encode())
        elif command=='switchdealer':
            if token!=game.dealer.token:
                return
            if not game.autodeal:
                if game.dealer.name=='שירי':
                    for i in game.players:
                        if i.name=='רמון':
                            game.dealer=i
                elif game.dealer.name=='רמון':
                    for i in game.players:
                        if i.name=='שירי':
                            game.dealer=i
        elif command=='autodeal':
            game.autodeal=True
        elif command=='dealer':
            if token in game.tp:
                game.autodeal=False
                game.dealer=game.players[game.tp[token]]
            else:
                i_s=[i for i in range(4) if players[i].name=='שירי']
                if i_s!=[]:
                    game.autodeal=False
                    game.dealer=game.players[i_s[0]]
        elif command=='event':
            s=""
            if game==None:
                try:
                    q=tokens[token]
                except:
                    time.sleep(60)
            elif token in game.observers:
                q=game.observers[token]
            elif token in game.tp:
                q=game.players[game.tp[token]].queue
                game.players[game.tp[token]].queue_waiting+=1
            try:
                s=q.get(timeout=60)
            except:
                s=''
            if game and token in game.tp:
                game.players[game.tp[token]].queue_waiting-=1
                pdebug(" waiting ", game.players[game.tp[token]].queue_waiting, "for ",token)
            try:
                self.wfile.write(s.encode())
                pdebug(" -> ", token,"   ",s)
            except Exception as e:
                pdebug(e)
                if s!='':
                    time.sleep(1)
                    q.put(s)
                    pdebug('Failed sending ',s,' to ',token,'. Trying again.')

port=8000
listen='localhost'
debug=False
logfile=sys.stderr

opts, args = getopt.getopt(sys.argv[1:],"D:Ldl:p:")

for o,a in opts:
    if o=='-p':
        port=int(a)
    if o=='-l':
        listen=a
    if o=='-L':
        listen=''
    if o=='-d':
        debug=True
    if o=='-D':
        debug=True
        logfile=open(a,"a")
        print('Debug on.', file=logfile)

httpd = ThreadingHTTPServer((listen,port), WhistHTTPRequestHandler)
sslctx = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
sslctx.check_hostname = False # If set to True, only the hostname that matches the certificate will be accepted
sslctx.load_cert_chain(certfile='/etc/letsencrypt/live/svgalib.org/cert.pem', keyfile="/etc/letsencrypt/live/svgalib.org/privkey.pem")
httpd.socket = sslctx.wrap_socket(httpd.socket, server_side=True)
httpd.serve_forever()

